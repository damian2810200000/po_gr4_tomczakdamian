public class IntegerSet
{
    private static int size = 100;
    boolean numbers[];

    IntegerSet()
    {
        numbers = new boolean[size];
    }

    public static IntegerSet union(IntegerSet firstCollection, IntegerSet secondCollection)
    {
        IntegerSet result = new IntegerSet();
        for (int i = 0; i < size; i++)
        {
            if (firstCollection.numbers[i] || secondCollection.numbers[i])
            {
                result.numbers[i] = true;
            }
        }
        return result;
    }

    public static IntegerSet intersection(IntegerSet firstCollection, IntegerSet secondCollection)
    {
        IntegerSet result = new IntegerSet();
        for (int i = 0; i < size; i++)
        {
            if (firstCollection.numbers[i] && secondCollection.numbers[i])
            {
                result.numbers[i] = true;
            }
        }
        return result;
    }

    public void insertElement(int number) throws RuntimeException
    {
        if (number < 1 || number > 100)
            throw new RuntimeException("The number is beyond the index: " + number);
        this.numbers[number-1] = true;
    }

    public void deleteElement(int number)
    {
        if (number < 1 || number > 100)
            throw new RuntimeException("The number is beyond the index: " + number);
        this.numbers[number-1] = false;
    }

    @Override
    public String toString()
    {
        String result = new String();
        for (int i = 0; i < size; i++)
        {
            if (this.numbers[i])
                result += i+1 + " ";
        }
        return result.length() == 0 ? "The list is empty" : result;
    }

    @Override
    public boolean equals(Object collection)
    {
        IntegerSet comparison = (IntegerSet)collection;
        for (int i = 0; i < size; i++)
        {
            boolean tmp1 = this.numbers[i];
            boolean tmp2 = comparison.numbers[i];

            if ((this.numbers[i] && !comparison.numbers[i]) ||
                !numbers[i] && comparison.numbers[i])
                return false;
        }
        return true;
    }
}
