public class Main
{
    public static void main(String[] args)
    {
        RachunekBankowy saver1 = new RachunekBankowy(2000.0);
        RachunekBankowy saver2 = new RachunekBankowy(3000.0);
        RachunekBankowy.rocznaStopaProcentowa = 0.04;
        saver1.obliczMiesieczneOdsetki();
        saver2.obliczMiesieczneOdsetki();
        System.out.println("Saver1: " + saver1.getOszczednosci());
        System.out.println("Saver2: " + saver2.getOszczednosci());
        RachunekBankowy.rocznaStopaProcentowa = 0.05;
        saver1.obliczMiesieczneOdsetki();
        saver2.obliczMiesieczneOdsetki();
        System.out.println("Saver1: " + saver1.getOszczednosci());
        System.out.println("Saver2: " + saver2.getOszczednosci());

        System.out.println("----------------------------------");
        IntegerSet set1 = new IntegerSet();
        IntegerSet set2 = new IntegerSet();
        System.out.println(set1);
        System.out.println(set2);
        set1.insertElement(100);
        set2.insertElement(100);
        set2.insertElement(1);
        System.out.println(set1);
        System.out.println(set2);
        if (set1.equals(set2))
        {
            System.out.println("Sets are the same!");
        }
        else
        {
            System.out.println("Sets are not the same!");
        }
        set2.deleteElement(1);
        System.out.println(set1);
        System.out.println(set2);
        if (set1.equals(set2))
        {
            System.out.println("Sets are the same!");
        }
        else
        {
            System.out.println("Sets are not the same!");
        }
        set1.insertElement(1);
        set1.insertElement(2);
        set1.insertElement(3);
        set2.insertElement(2);
        set2.insertElement(3);
        set2.insertElement(4);
        System.out.println(set1);
        System.out.println(set2);
        IntegerSet _new1 = IntegerSet.union(set1, set2);
        System.out.println("Union: " + _new1);
        IntegerSet _new2 = IntegerSet.intersection(set1, set2);
        System.out.println("Intersection: " + _new2);

    }
}
