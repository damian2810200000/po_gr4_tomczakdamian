public class RachunekBankowy
{
    static double rocznaStopaProcentowa;
    private double oszczednosci;

    RachunekBankowy(double baseline)
    {
        this.oszczednosci = baseline;
    }

    public void obliczMiesieczneOdsetki()
    {
        this.oszczednosci += (oszczednosci * rocznaStopaProcentowa) / 12;
    }

    static void setRocznaStopaProcentowa(double newValue)
    {
        rocznaStopaProcentowa = newValue;
    }

    double getOszczednosci()
    {
        return oszczednosci;
    }
}
