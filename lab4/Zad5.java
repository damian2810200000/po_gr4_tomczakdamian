import javax.swing.plaf.metal.MetalCheckBoxIcon;
import java.math.BigInteger;
import java.math.BigDecimal;

public class Zad5
{
    public static void main(String[] args)
    {
        BigDecimal k = new BigDecimal(args[0]);
        BigDecimal p = new BigDecimal(Double.valueOf(args[1]));
        BigDecimal n = new BigDecimal(args[2]);

        BigDecimal result = k;

        for (BigInteger i = new BigInteger("0");
             i.compareTo(n.toBigInteger()) != 1;
             i = i.add(new BigInteger("1")))
        {
            result = result.add(result.multiply(p));
        }

        System.out.println(result.toString());


    }
}
