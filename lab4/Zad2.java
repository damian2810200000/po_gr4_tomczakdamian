import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Zad2
{
    public static void main(String[] args)
    {
        try
        {
            File file = new File(args[0]);
            Scanner scanner = new Scanner(file);
            String data = new String();
            while (scanner.hasNextLine())
            {
               data += scanner.nextLine();
            }
            System.out.println(counterChar(data, args[1].charAt(0)));
        }
        catch (FileNotFoundException e)
        {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }

    static int counterChar(String str, char c)
    {
        int result = 0;
        for (char i : str.toCharArray())
        {
            if ( i == c)
                result++;
        }
        return result;
    }
}
