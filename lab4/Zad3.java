import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Zad3
{
    public static void main(String[] args)
    {
        try
        {
            File file = new File(args[0]);
            Scanner scanner = new Scanner(file);
            String data = new String();
            while (scanner.hasNextLine())
            {
                data += scanner.nextLine();
            }
            System.out.println(countSubStr(data, args[1]));
        }
        catch (FileNotFoundException e)
        {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }

    static int countSubStr(String str, String subStr)
    {
        int result = 0;
        for (int i = 0; i < str.length(); i++)
        {
            int j = 0;
            for (char c : subStr.toCharArray())
            {
                if (str.charAt(i + j) != c)
                    break;
                else
                    j++;
            }
            if (j == subStr.length())
                result++;
        }
        return result;
    }
}
