import java.math.BigInteger;

public class Zad4
{
    public static void main(String[] args)
    {
        int n = Integer.parseInt(args[0]);
        BigInteger[][] board = new BigInteger[n][n];

        BigInteger seeds = new BigInteger("1");
        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < n; j++)
            {
                board[i][j] = seeds;
                seeds = seeds.multiply(new BigInteger("2"));
            }
        }

        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < n; j++)
            {
                System.out.print(board[i][j] + " ");
            }
            System.out.println();
        }

        BigInteger result = new BigInteger("0");
        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < n; j++)
            {
                result = result.add(board[i][j]);
            }
        }
        System.out.println("Result: " + result);

    }
}
