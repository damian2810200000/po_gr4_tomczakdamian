import java.util.Arrays;
import java.util.Scanner;
import java.util.Random;

public class Zad1
{
    public static void main(String[] args)
    {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Type word: ");
        String str1 = scanner.nextLine();
        String str2 = scanner.nextLine();
        String str3 = scanner.nextLine();
//        String str1 = new String();
//        String str2 = new String();
//        String str3 = new String();
//        str1 = "Damian Lucja Damian Sebastian Tomasz";
//        str2 = "Damian";
//        str3 = "9234190848342094382349";
        char c = 'a';

        System.out.println("a) " + counterChar(str1, c));

        System.out.println("b) " + countSubStr(str1, str2));

        System.out.println("c) " + middle(str2));

        System.out.println("d) " + repeat("ho", 3));

        System.out.print("e) ");
        int[] resultE = where(str1, str2);
        for (int i : resultE)
        {
            System.out.print(i + " ");
        }

        System.out.println("\nf) " + change(str2));

        System.out.println("g) " + nice(str3));

        System.out.println("h) " + niceModified(str3, '$', 4));
    }

    static int counterChar(String str, char c)
    {
        int result = 0;
        for (char i : str.toCharArray())
        {
            if ( i == c)
                result++;
        }
        return result;
    }

    static int countSubStr(String str, String subStr) {
        int result = 0;
        for (int i = 0; i < str.length(); i++) {
            int tmp = 0;
            for (int j = 0; j<subStr.length() && i+j<str.length(); j++) {
                if (str.charAt(i + j) != subStr.charAt(j))
                    break;
                else
                    tmp++;
            }
            if (tmp == subStr.length())
                result++;
        }
        return result;
    }

    static String middle(String str)
    {
        int l = str.length();
        return l % 2 != 0 ? String.valueOf(str.charAt(l/2 - 1))
                : String.valueOf(str.charAt(l/2 - 1)) + String.valueOf(str.charAt(l/2));
    }
    
    static String repeat(String str, int n)
    {
        String result = new String();
        for (int i = 0; i < n; i++)
        {
            result += str;
        }
        return result;
    }

    static int[] where(String str, String subStr)
    {
        int[] result = new int[0];
        for (int i = 0, z = 0; i < str.length(); i++)
        {
            int tmp = 0
            for (int j = 0; j<subStr.length() && i+j<str.length(); j++)
            {
                if (str.charAt(i + j) != subStr.charAt(j))
                    break;
                else
                    tmp++;
            }
            if (tmp == subStr.length())
            {
                result = Arrays.copyOf(result, result.length+1);
                result[z++] = i;
            }
        }
        return result;
    }

    static String change(String str)
    {
        StringBuffer strb = new StringBuffer();
        strb.append(str);

        for (int i = 0; i < strb.length(); i++)
        {
            char c = strb.charAt(i);
            strb.setCharAt(i, Character.isUpperCase(c)
                    ? Character.toLowerCase(c) : Character.toUpperCase(c));
        }
        return strb.toString();
    }

    static String nice(String str)
    {
        StringBuffer strb = new StringBuffer();

        boolean first = true;
        for (int i = str.length() - 1, j = 0; i >= 0; i--, j++)
        {
            if (j % 3 == 0 && !first)
            {
                strb.insert(0, ',');
            }
            first = false;
            strb.insert(0, str.charAt(i));
        }
        return strb.toString();
    }

    static String niceModified(String str, char sign, int counter)
    {
        StringBuffer strb = new StringBuffer();

        boolean first = true;
        for (int i = str.length() - 1, j = 0; i >= 0; i--, j++)
        {
            if (j % counter == 0 && !first)
            {
                strb.insert(0, sign);
            }
            first = false;
            strb.insert(0, str.charAt(i));
        }
        return strb.toString();
    }
}
