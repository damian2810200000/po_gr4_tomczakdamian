import java.util.Scanner;

public class Zadanie1_1_a {

    public static void main(String[] args) 
    {
        Scanner scan = new Scanner(System.in);
        System.out.println("Podaj liczbe n: ");
        int n = Integer.parseInt(scan.nextLine());

        double result = 0;

        for(int i = 1; i <= n; i++)
        {
            System.out.println("Podaj kolejną liczbę: ");
            double ai = Double.parseDouble(scan.nextLine());

            result += ai;
        }

        System.out.println("Wynik: " + result);
    }
}
