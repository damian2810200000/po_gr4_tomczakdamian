import java.util.Scanner;

public class Zadanie2_4 {

    public static void main(String[] args) 
    {
        Scanner scan = new Scanner(System.in);
        System.out.println("Podaj liczbe n: ");
        int n = Integer.parseInt(scan.nextLine());
        double resultPlus = 0;
        double resultMinus = 0;
        double[] array = new double[n];

        for(int i = 0; i < n; i++)
        {
            array[i] = Double.parseDouble(scan.nextLine());
        }

        for(int i = 0; i < n; i++)
        {
            if (array[i] > resultPlus)
                resultPlus = array[i];
            if (array[i] < resultMinus)
                resultMinus = array[i];
        }

        System.out.println("Result+: " + resultPlus);
        System.out.println("Result-: " + resultMinus);
    }
}
