
import java.util.Scanner;

public class Zadanie1_2 {

    public static void main(String[] args) 
    {
        Scanner scan = new Scanner(System.in);
        System.out.println("Podaj liczbe n: ");
        int n = Integer.parseInt(scan.nextLine());
        double[] array = new double[n];

        for(int i = 0; i < n; i++)
        {
            double ai = Double.parseDouble(scan.nextLine());
            array[i] = ai;
        }

        for(int i = 1; i < n; i++)
        {
            System.out.println(array[i]);
        }
        System.out.println(array[0]);
    }
}
