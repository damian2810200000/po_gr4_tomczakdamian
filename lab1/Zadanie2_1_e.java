import java.util.Scanner;

public class Zadanie2_1_e {

    public static void main(String[] args) 
    {
        Scanner scan = new Scanner(System.in);
        System.out.println("Podaj liczbe n: ");
        int n = Integer.parseInt(scan.nextLine());
        double[] array = new double[n];
        int result = 0;

        for(int i = 0; i < n; i++)
        {
            double ai = Double.parseDouble(scan.nextLine());

            if ((ai > Math.pow(2, i+1)) && (ai < factorial(i)))
                result++;
        }

        System.out.println("Result: " + result);
    }
    
    public static double factorial(double number)
    {
        double result = 1;
        for(int i = 1; i <= number; i++)
        {
            result *= i;
        }

        return result;
    }
}
