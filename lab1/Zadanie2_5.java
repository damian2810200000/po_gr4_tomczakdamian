import java.util.Scanner;

public class Zadanie2_5 {

    public static void main(String[] args) 
    {
        Scanner scan = new Scanner(System.in);
        System.out.println("Podaj liczbe n: ");
        int n = Integer.parseInt(scan.nextLine());
        double[][] result = new double[n-1][2];
        double[] array = new double[n];
        int resultI = 0;

        for(int i = 0; i < n; i++)
        {
            array[i] = Double.parseDouble(scan.nextLine());
        }

        for(int i = 0; i < n-1; i++)
        {
            if(array[i]>0 && array[i+1]>0)
            {
                result[resultI][0] = array[i];
                result[resultI][1] = array[i+1];
                resultI++;
            }
        }

        System.out.println("Pairs(" + resultI + "): \n");
        for(int i = 0; i < resultI; i++)
        {
            System.out.println("(" + result[i][0] + ", " + result[i][1] + ")");
        }
    }
}
