import java.util.Scanner;

public class Zadanie1_1_g {

    public static void main(String[] args) 
    {
        Scanner scan = new Scanner(System.in);
        System.out.println("Podaj liczbe n: ");
        int n = Integer.parseInt(scan.nextLine());
        double[] array = new double[n];

        double result = 0;

        for(int i = 0; i < n; i++)
        {
            System.out.println("Podaj kolejną liczbę: ");
            double ai = Double.parseDouble(scan.nextLine());
            array[i] = ai;
        }

        for (int i = 0; i < n; i++)
        {
            result += array[i];
        }

        System.out.println("Wynik1: " + result);

        result = 1;

        for (int i = 0; i < n; i++)
        {
            result *= array[i];
        }

        System.out.println("Wynik2: " + result);
    }
}
