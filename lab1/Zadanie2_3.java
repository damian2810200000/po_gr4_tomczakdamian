import java.util.Scanner;

public class Zadanie2_3 {

    public static void main(String[] args) 
    {
        Scanner scan = new Scanner(System.in);
        System.out.println("Podaj liczbe n: ");
        int n = Integer.parseInt(scan.nextLine());
        int resultPlus = 0;
        int resultMinus = 0;
        int resultZero = 0;

        for(int i = 0; i < n; i++)
        {
            double ai = Double.parseDouble(scan.nextLine());

            if(ai > 0)
                resultPlus++;
            else if (ai == 0)
                resultZero++;
            else
                resultMinus++;
        }

        System.out.println("Result+: " + resultPlus);
        System.out.println("Result=0: " + resultZero);
        System.out.println("Result-: " + resultMinus);
    }
}
