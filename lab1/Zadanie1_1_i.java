import java.util.Scanner;

public class Zadanie1_1_i {

    public static void main(String[] args) 
    {
        Scanner scan = new Scanner(System.in);
        System.out.println("Podaj liczbe n: ");
        int n = Integer.parseInt(scan.nextLine());

        double result = 0;

        for(int i = 1; i <= n; i++)
        {
            System.out.println("Podaj kolejną liczbę: ");
            double ai = Double.parseDouble(scan.nextLine());

            double nominator = Math.pow(-1, i) * ai;
            double denominator = factorial(i);
            result += nominator / denominator;
        }

        System.out.println("Wynik: " + result);
    }
    
    public static double factorial(double number)
    {
        double result = 1;
        for(int i = 1; i <= number; i++)
        {
            result *= i;
        }

        return result;
    }
}
