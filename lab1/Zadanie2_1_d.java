import java.util.Scanner;

public class Zadanie2_1_d {

    public static void main(String[] args) 
    {
        Scanner scan = new Scanner(System.in);
        System.out.println("Podaj liczbe n: ");
        int n = Integer.parseInt(scan.nextLine());
        double[] array = new double[n];
        int result = 0;

        for(int i = 0; i < n; i++)
        {
            double ai = Double.parseDouble(scan.nextLine());

            array[i] = ai;
        }

        for(int i = 1; i < n-1; i++)
        {
            double calculation = (array[i - 1] + array[i + 1]) / 2;
            if (array[i] < calculation)
                result++;
        }

        System.out.println("Result: " + result);
    }
}
