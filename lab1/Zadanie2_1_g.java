import java.util.Scanner;

public class Zadanie2_1_g {

    public static void main(String[] args) 
    {
        Scanner scan = new Scanner(System.in);
        System.out.println("Podaj liczbe n: ");
        int n = Integer.parseInt(scan.nextLine());
        double[] array = new double[n];
        int result = 0;

        for(int i = 0; i < n; i++)
        {
            double ai = Double.parseDouble(scan.nextLine());

            if ((ai > 0) && (ai % 2 != 0))
                result++;
        }
        
        System.out.println("Result: " + result);
    }
}
