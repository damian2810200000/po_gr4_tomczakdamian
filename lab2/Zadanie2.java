import java.util.Random;
import java.util.Scanner;

public class Main
{
    public static void main (String[] args)
    {
        Scanner scan = new Scanner(System.in);
        int n = Integer.parseInt(scan.nextLine());
        if ((n < 1) || (n > 100))
        {
            throw new FooRuntimeException("n = ", n);
        }
        int[] array = new int[n];

        generuj(array, n, -999, 999);
        wypisz(array);
        System.out.print("Ile nieparzystych: " + ileNieparzystych(array) + "\n");
        System.out.print("Ile parzystych: " + ileParzystych(array) + "\n");

        System.out.print("Ile dodatnich: " + ileDodatnich(array) + "\n");
        System.out.print("Ile ujemnych: " + ileUjemnych(array) + "\n");
        System.out.print("Ile zerowych: " + ileZerowych(array) + "\n");

        System.out.print("Ile maksymalnych: " + ileMaksymalnych(array) + "\n");

        System.out.print("Suma dodatnich: " + sumaDodatnich(array) + "\n");
        System.out.print("Suma ujemnych: " + sumaUjemnych(array) + "\n");

        System.out.print("Dlugosc maksymalnego ciagu dodatnich: "
                + dlugoscMaksymalnegoCiaguDodatnich(array) + "\n");

        odwrocFragment(array, 2, 5);

        signum(array);
    }

    public static void generuj (int[] tab, int n, int min, int max)
    {
        Random rand = new Random();
        for (int i = 0; i < n; i++)
        {
            int howMany = max - min;
            int value = rand.nextInt(howMany) + min;

            // START::ONLY FOR DEBUG
            if ((value < -999) || (value > 999))
            {
                throw new FooRuntimeException("value = ", value);
            }
            // END::ONLY FOR DEBUG
            tab[i] = value;
        }
    }

    public static void wypisz(int[] tab)
    {
        System.out.print("Array: ");
        for (int item : tab)
        {
            System.out.print(item + " ");
        }
        System.out.println("");
    }

    public static int ileNieparzystych (int tab[])
    {
        int result = 0;

        for (int item : tab)
        {
            if (item % 2 != 0)
                result++;
        }
        return result;
    }

    public static int ileParzystych (int tab[])
    {
        int result = 0;

        for (int item : tab)
        {
            if (item % 2 == 0)
                result++;
        }
        return result;
    }

    public static int ileDodatnich (int tab[])
    {
        int result = 0;

        for (int item : tab)
        {
            if (item > 0)
                result++;
        }

        return result;
    }

    public static int ileUjemnych (int tab[])
    {
        int result = 0;

        for (int item : tab)
        {
            if (item < 0)
                result++;
        }

        return result;
    }

    public static int ileZerowych (int tab[])
    {
        int result = 0;

        for (int item : tab)
        {
            if (item == 0)
                result++;
        }

        return result;
    }

    public static int ileMaksymalnych (int tab[])
    {
        int result = tab[0];
        int howMany = 0;

        for (int item : tab)
        {
            if (item == result)
                howMany++;
            else if (item > result)
            {
                result = item;
                howMany = 1;
            }
        }
        return howMany;
    }

    public static int sumaDodatnich (int tab[])
    {
        int result = 0;

        for (int item : tab)
        {
            if (item > 0)
                result += item;
        }

        return result;
    }

    public static int sumaUjemnych (int tab[])
    {
        int result = 0;

        for (int item : tab)
        {
            if (item < 0)
                result += item;
        }

        return result;
    }

    public static int dlugoscMaksymalnegoCiaguDodatnich (int tab[])
    {
        int resultMax = 0;
        int result = 0;

        for (int i = 0; i < tab.length; i++)
        {
            result = 0;
            for (int j = i; j < tab.length; j++)
            {
                if (tab[j] > 0)
                    result++;
                else
                {
                    break;
                }
            }
            if (result > resultMax)
                resultMax = result;
        }
        return resultMax;
    }

    public static void signum(int tab[])
    {
        for (int i = 0; i < tab.length; i++)
        {
            if (tab[i] > 0)
                tab[i] = 1;
            else if (tab[i] < 0)
                tab[i] = -1;
        }

        wypisz(tab);
    }

    public static void odwrocFragment(int tab[], int lewy, int prawy)
    {
        wypisz(tab);

        int max = Math.max(lewy, prawy);
        int min = Math.min(lewy, prawy);

        System.out.println("left: " + lewy);
        System.out.println("right: " + prawy);

        for (int i = 0; i < (max - min + 1) / 2; i++)
        {
            int tmp = tab[min + i];
            tab[min + i] = tab[max - i];
            tab[max - i] = tmp;
        }

        wypisz(tab);
    }
}

