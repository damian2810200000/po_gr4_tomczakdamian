import java.util.Random;
import java.util.Scanner;
import java.lang.Math;

public class Main {

    public static void main(String[] args)
    {
        Random rand;
        Scanner scan;
        int[] values ;
        int n;

        scan = new Scanner(System.in);
        rand = new Random();
        n = Integer.parseInt(scan.nextLine());
        values = new int[n];

        if ((n < 1) || (n > 100))

        {
            throw new FooRuntimeException("n = ", n);
        }

        for (int i = 0; i < n; i++)
        {
            int value = rand.nextInt(1999) - 999;

            if ((value < -999) || (value > 999)) {
                throw new FooRuntimeException("value = ", value);
            }
            values[i] = value;
        }

        //////////////////////////////////////////////////////////////////
        for (int i = 0; i < n; i++)
        {
            System.out.print(values[i] + ", ");
        }
        System.out.print("\n");

        int lewy = rand.nextInt(n);
        int prawy = rand.nextInt(n);
        int max = Math.max(lewy, prawy);
        int min = Math.min(lewy, prawy);

        System.out.println("left: " + lewy);
        System.out.println("right: " + prawy);

        for (int i = 0; i < (max - min + 1) / 2; i++)
        {
            int tmp = values[min + i];
            values[min + i] = values[max - i];
            values[max - i] = tmp;
        }

        for (int i = 0; i < n; i++)
        {
            System.out.print(values[i] + ", ");
        }
    }
    
}
