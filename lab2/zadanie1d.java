import java.util.Random;
import java.util.Scanner;

public class Main {

    public static void main(String[] args)
    {
        Random rand;
        Scanner scan;
        int[] values ;
        int n;

        scan = new Scanner(System.in);
        rand = new Random();
        n = Integer.parseInt(scan.nextLine());
        values = new int[n];

        if ((n < 1) || (n > 100))

        {
            throw new FooRuntimeException("n = ", n);
        }

        for (int i = 0; i < n; i++)
        {
            int value = rand.nextInt(1999) - 999;

            if ((value < -999) || (value > 999)) {
                throw new FooRuntimeException("value = ", value);
            }
            values[i] = value;
        }
        int resultPositve = 0;
        int resultNegative = 0;

        for (int i = 0; i < n; i++)
        {
            if (values[i] > 0)
                resultPositve += values[i];
            else
                resultNegative += values[i];
        }
        System.out.println("Positive sum: " + resultPositve);
        System.out.println("Negative sum: " + resultNegative);
    }
    
}
