public class FooRuntimeException extends RuntimeException
{   
    public FooRuntimeException(String message, int n) 
    { 
        super(message + String.valueOf(n)); 
    }
    public FooRuntimeException(String message, double n) 
    { 
        super(message + String.valueOf(n)); 
    }
}
