import java.util.Random;
import java.util.Scanner;
import java.lang.Math;

public class Main {

    public static void main(String[] args)
    {
        Random rand = new Random();
        int m = rand.nextInt(10) + 1;
        int n = rand.nextInt(10) + 1;
        int k = rand.nextInt(10) + 1;
        int[][] a = new int[m][n];
        int[][] b = new int[n][k];
        int[][] c = new int[m][k];
        fill(a, m, n);
        fill(b, n, k);
        print(a, m, n);
        multiPrint(n, k);
        print(b, n, k);
        equalPrint(n, k);
        multi(a, b, c, m, n, n, k);
        print(c, m, k);
    }

    static void fill(int[][] matrix, int rows, int columns)
    {
        Random rand = new Random();
        for (int i = 0; i < rows; i++)
        {
            for(int j = 0; j < columns; j++)
            {
                matrix[i][j] = rand.nextInt(10);
            }
        }
    }

    static void print(int[][] matrix, int rows, int columns)
    {
        for (int i = 0; i < rows; i++)
        {
            System.out.print("[");
            for(int j = 0; j < columns; j++)
            {
                System.out.print(matrix[i][j] + " ");
            }
            System.out.print("]\n");
        }
    }

    static void multiPrint(int columns1, int columns2)
    {
        int max = Math.max(columns1, columns2);
        System.out.print(" ");
        for (int i = 0; i < max; i++)
        {
            if (i == max / 2)
                System.out.print("* ");
            else
                System.out.print("  ");
        }
        System.out.print(" \n");
    }

    static void equalPrint(int columns1, int columns2)
    {
        int max = Math.max(columns1, columns2);
        System.out.print(" ");
        for (int i = 0; i < max; i++)
        {
            if (i == max / 2)
                System.out.print("= ");
            else
                System.out.print("  ");
        }
        System.out.print(" \n");
    }

    static void multi(int[][] first,
                      int[][] second,
                      int[][] thrid,
                      int rows1,
                      int columns1,
                      int rows2,
                      int columns2)
    {
        for (int i = 0; i < rows1; i++)
        {
            for (int j = 0; j < columns2; j++)
            {
                for (int p = 0; p < rows2; p++)
                {
                    thrid[i][j] += first[i][p] * second[p][j];
                }
            }
        }
    }
}
