import pl.imiajd.tomczak.Adres;
import pl.imiajd.tomczak.BetterRectangle;
import pl.imiajd.tomczak.Nauczyciel;
import pl.imiajd.tomczak.Student;
import pl.imiajd.tomczak.Osoba;

public class Main
{
    public static void main(String[] args)
    {
        Adres adres1 = new Adres("Stacha Konwy", "35A", "Zabrodzie","07-410");
        Adres adres2 = new Adres("Stacha Konwy", "30", 1, "Ostrołęka","07-400");

        adres1.pokaz();
        adres2.pokaz();
        System.out.println(adres1.przed(adres2));
        System.out.println();

        Student student1 = new Student("Tomczak", 2000, "WSMIRH");
        System.out.print(student1);
        System.out.println();

        Nauczyciel nauczyciel1 = new Nauczyciel("Tomczak", 2000, 666);
        System.out.print(nauczyciel1);
        System.out.println(nauczyciel1.getNazwisko());
        System.out.println();

        Osoba osoba1 = new Osoba("Tomczak", 2000);
        System.out.print(osoba1);
        System.out.println(osoba1.getNazwisko());

        System.out.println();
        BetterRectangle rectangle = new BetterRectangle(0, 0, 10, 15);
        System.out.println(rectangle.getPerimeter());
        System.out.println(rectangle.getArea());
    }

}
