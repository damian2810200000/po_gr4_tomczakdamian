package pl.imiajd.tomczak;

public class Student extends Osoba
{
    private String kierunek;

    public Student(String _nazwisko, int _rokUrodzenia, String _kierunek)
    {
        super(_nazwisko, _rokUrodzenia);
        this.kierunek = _kierunek;
    }

    public String getKierunek()
    {
        return this.kierunek;
    }

    @Override
    public String toString()
    {
        return super.toString() + this.kierunek + "\n";
    }
}
