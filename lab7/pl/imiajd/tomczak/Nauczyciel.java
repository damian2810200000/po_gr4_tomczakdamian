package pl.imiajd.tomczak;

public class Nauczyciel extends Osoba
{
    private int pensja;

    public Nauczyciel(String _nazwisko, int _rokUrodzenia, int _pensja)
    {
        super(_nazwisko, _rokUrodzenia);
        this.pensja = _pensja;
    }

    @Override
    public String toString()
    {
        return super.toString() + this.pensja + "\n";
    }
}
