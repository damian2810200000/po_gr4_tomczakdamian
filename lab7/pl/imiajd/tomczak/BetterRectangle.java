package pl.imiajd.tomczak;

import java.awt.Rectangle;

public class BetterRectangle extends Rectangle
{
    public BetterRectangle(int _x, int _y, int _width, int _height)
    {
//        super.setLocation(_x, _y);
//        super.setSize(_width, _height);
        super(_x, _y, _width, _height);
    }

    public int getPerimeter()
    {
        return super.width * 2 + super.height * 2;
    }

    public int getArea()
    {
        return super.width * super.height;
    }
}
