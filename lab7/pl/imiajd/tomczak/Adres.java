package pl.imiajd.tomczak;

public class Adres
{
    private String ulica;
    private String numer_domu;
    private int numer_mieszania;
    private String miasto;
    private String kod_pocztowy;

    public Adres(String _ulica, String _numer_domu, int _numer_mieszkania, String _miasto, String _kodpocztowy)
    {
        this.ulica = _ulica;
        this.numer_domu = _numer_domu;
        this.numer_mieszania = _numer_mieszkania;
        this.miasto = _miasto;
        this.kod_pocztowy = _kodpocztowy;
    }

    public Adres(String _ulica, String _numer_domu, String _miasto, String _kodpocztowy)
    {
        this.ulica = _ulica;
        this.numer_domu = _numer_domu;
        this.miasto = _miasto;
        this.kod_pocztowy = _kodpocztowy;
    }

    public void pokaz()
    {
        System.out.println(this.kod_pocztowy + " " + this.miasto);
        System.out.print(this.numer_domu + " " + this.ulica + " ");
        if (this.numer_mieszania != 0)
        {
            System.out.print(this.numer_mieszania + "\n");
        }
        else
        {
            System.out.print("\n");
        }
    }

    public boolean przed(Adres _adres) throws RuntimeException
    {
        if (!this.kod_pocztowy.substring(0, 2).equals(_adres.kod_pocztowy.substring(0, 2)))
            throw new RuntimeException("The addresses are in other cities.");

        if (this.kod_pocztowy.length() > 6 || _adres.kod_pocztowy.length() > 6)
            throw new RuntimeException("Addresses are incorrect.");

        if (Integer.parseInt(this.kod_pocztowy.substring(3, 6)) < Integer.parseInt(_adres.kod_pocztowy.substring(3, 6)))
            return true;
        else
            return false;
    }


}
