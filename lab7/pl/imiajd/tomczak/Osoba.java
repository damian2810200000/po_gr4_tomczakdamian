package pl.imiajd.tomczak;

public class Osoba
{
    private String nazwisko;
    private int rokUrodzenia;

    public Osoba(String _nazwisko, int _rokUrodzenia)
    {
        this.nazwisko = _nazwisko;
        this.rokUrodzenia = _rokUrodzenia;
    }

    public int getRokUrodzenia()
    {
        return this.rokUrodzenia;
    }

    public String getNazwisko()
    {
        return this.nazwisko;
    }

    @Override
    public String toString()
    {
        return this.nazwisko + "\n" + this.rokUrodzenia + "\n";
    }
}
