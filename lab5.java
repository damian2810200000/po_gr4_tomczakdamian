import java.util.ArrayList;
import java.util.Arrays;

public class Main
{
    public static void main(String[] args)
    {
        ArrayList<Integer> a = new ArrayList<Integer>(Arrays.asList(1,4,9,16));
        ArrayList<Integer> b = new ArrayList<Integer>(Arrays.asList(9,7,4,9,11));
        System.out.println("Zadanie1: ");
        System.out.println(append(a, b));

        System.out.println("Zadanie2: ");
        System.out.println(merge(b, a));

        System.out.println("Zadanie3: ");
        System.out.println(mergeSorted(a, b));

        System.out.println("Zadanie4: " + b);
        ArrayList<Integer> zadanie4 = reversed(b);
        System.out.println(reversed_check(zadanie4, b));
        System.out.println("Zadanie4: " + zadanie4);

        System.out.println("Zadanie5: " + b);
        ArrayList<Integer> beforeReverse = new ArrayList<Integer>(b);
        reverse(b);
        System.out.println(reversed_check(b, beforeReverse));
        System.out.println("Zadanie5: " + b);
    }

    public static ArrayList<Integer> append(ArrayList<Integer> a, ArrayList<Integer> b)
    {
        int size = a.size() + b.size();
        ArrayList<Integer> result = new ArrayList<Integer>(Arrays.asList(new Integer[size]));
        for (int i = 0; i < a.size(); i++)
        {
            result.set(i, a.get(i));
        }
        for (int i = 0; i < b.size(); i++)
        {
            result.set(i + a.size(), b.get(i));
        }
        return result;
    }

    public static ArrayList<Integer> merge(ArrayList<Integer> a, ArrayList<Integer> b)
    {
        ArrayList<Integer> result = new ArrayList<Integer>(Arrays.asList(new Integer[a.size() + b.size()]));
        for (int i = 0, j = 0; i < result.size(); j++)
        {
            if (j < a.size())
            {
                result.set(i, a.get(j));
                i++;
            }
            if (j < b.size())
            {
                result.set(i, b.get(j));
                i++;
            }
        }
        return result;
    }

    public static ArrayList<Integer> mergeSorted(ArrayList<Integer> a, ArrayList<Integer> b)
    {
        ArrayList<Integer> appended = new ArrayList<Integer>(append(a,b));
        ArrayList<Integer> sureResult = new ArrayList<Integer>();
        while (appended.size() != 0)
        {
            int min = appended.get(0);
            for (int j = 0; j < appended.size(); j++)
            {
                if (appended.get(j) < min)
                {
                    min = appended.get(j);
                }
            }
            appended.remove(Integer.valueOf(min));
            sureResult.add(min);

        }
        return sureResult;
    }

    public static ArrayList<Integer> reversed(ArrayList<Integer> a)
    {
        ArrayList<Integer> result = new ArrayList<Integer>(a);
        for (int i = 0; i < result.size()/2; i++)
        {
            int tmp = a.get(result.size()-1-i);
            result.set(result.size()-1-i, result.get(i));
            result.set(i, tmp);
        }
        return result;
    }

    public static void reverse(ArrayList<Integer> a)
    {
        for (int i = 0; i < a.size()/2; i++)
        {
            int tmp = a.get(a.size()-1-i);
            a.set(a.size()-1-i, a.get(i));
            a.set(i, tmp);
        }
    }

    public static boolean reversed_check(ArrayList<Integer> reversed, ArrayList<Integer> notreversed)
    {
        if (reversed.size() != notreversed.size())
        {
            return false;
        }
        else
        {
            for (int i = 0; i < reversed.size(); i++)
            {
                if (reversed.get(i) != notreversed.get(notreversed.size()-i-1))
                    return false;
            }
            return true;
        }
    }
}